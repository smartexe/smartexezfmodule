<?php

namespace SmartexeZFModule {

    use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\MvcEvent;

    class Module implements AutoloaderProviderInterface
    {

        public function onBootstrap(MvcEvent $event)
        {
            $log = new \Zend\Log\Logger();
            $log->addWriter(new \Zend\Log\Writer\Stream('./data/cronlog'));
            $sharedManager = $event->getApplication()->getEventManager()->getSharedManager();
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'minutelyAction', function($e) use ($log) {
                $log->info('End minutelyAction.');
            }, 1000);
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'hourlyAction', function($e) use ($log) {
                $log->info('End hourlyAction.');
            }, 1000);
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dailyAction', function($e) use ($log) {
                $log->info('End dailyAction.');
            }, 1000);
        }

        /**
         * This method is defined in ConsoleUsageProviderInterface
         */
        public function getConsoleUsage(\Zend\Console\Adapter\AdapterInterface $console)
        {
            return array(
                'cron <type> start' => 'Start cron event',
            );
        }

        public function getAutoloaderConfig()
        {
            return array(
                'Zend\Loader\StandardAutoloader' => array(
                    'namespaces' => array(
                        __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__),
                    ),
                ),
            );
        }

        public function getConfig()
        {
            return include __DIR__ . '/config/module.config.php';
        }

    }

}

namespace {

    date_default_timezone_set('UTC');

    /**
     * Wrapper for \Zend\Debug\Debug::dump
     *
     * @param mix $var
     */
    function var_d($someVar)
    {
        return \Zend\Debug\Debug::dump($someVar);
    }

    /**
     * Wrapper for \Zend\Debug\Debug::dump
     *
     * @param mix $var
     */
    function var_d2($someVar, $maxDepth = 2)
    {
        echo '<pre>';
        \Doctrine\Common\Util\Debug::dump($someVar, $maxDepth);
        echo '</pre>';
    }

    /**
     * Function universal check
     * @param mix $value
     * @return boolean 
     */
    function check(&$value)
    {
        if (is_array($value)) {
            return (isset($value) && count($value));
        } else {
            return (isset($value) && strlen($value));
        }
    }

}