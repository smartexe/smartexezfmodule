<?php
namespace SmartexeZFModule\Doctrine;

use Doctrine\ORM\EntityRepository;

class CommonRepository extends EntityRepository
{

    public function checkEntity($entity)
    {
        if (preg_match("|^[\d]+$|", $entity)) {
            $entity = $this->find($entity);
        }

        if ($entity instanceof \SmartexeZFModule\Doctrine\AbstractEntity) {
            return $entity;
        }

        return false;
    }

}