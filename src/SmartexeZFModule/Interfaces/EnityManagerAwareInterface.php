<?
namespace SmartexeZFModule\Interfaces;

interface EnityManagerAwareInterface
{

    public function setEntityManager($entityManager);
}