<?
namespace SmartexeZFModule\Interfaces;

interface ConfigAwareInterface
{

    public function setConfig($config);
}