<?php

namespace SmartexeZFModule\Service;

/**
 * Shell helper 
 */
class Shell
{

    /**
     * Exec command
     * 
     * @param $command - string command
     * 
     * @return string
     */
    public static function exec($command)
    {
        $randomNumber = rand(0, 1234567890);
        $outputFile = "/tmp/php_shell_output_{$randomNumber}";
        
        $out = shell_exec($command . " 2> {$outputFile}");

        if (is_null($out)) { //error
            return join("", file($outputFile));
        }

        return $out;
    }

}
