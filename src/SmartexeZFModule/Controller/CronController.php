<?php

namespace SmartexeZFModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class CronController extends AbstractActionController
{

    public function minutelyAction()
    {
        $this->events->trigger(__FUNCTION__);
    }

    public function hourlyAction()
    {
        $this->events->trigger(__FUNCTION__);
    }

    public function dailyAction()
    {
        $this->events->trigger(__FUNCTION__);
    }

}
