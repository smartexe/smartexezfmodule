<?php

return array(
    'service_manager' => array(
        'invokables' => array(
            'smrShell' => "SmartexeZFModule\Service\Shell"
        ),
        'initializers' => array(
            function ($instance, $sm) {
            if ($instance instanceof \SmartexeZFModule\Interfaces\ConfigAwareInterface) {
                $config = $sm->get('Config');
                $instance->setConfig($config);
            }
            if ($instance instanceof \SmartexeZFModule\Interfaces\EnityManagerAwareInterface) {
                $entityManager = $sm->get('doctrine.entitymanager.orm_default');
                $instance->setEntityManager($entityManager);
            }
        }
        ),
        'aliases' => array(
            'EntityManager' => 'doctrine.entitymanager.orm_default',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'SmartexeZFModule\Controller\Cron' => 'SmartexeZFModule\Controller\CronController'
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'cron' => array(
                    'options' => array(
                        'route' => 'cron <action> start',
                        'defaults' => array(
                            'controller' => 'SmartexeZFModule\Controller\Cron'
                        )
                    )
                )
            )
        )
    )
);
